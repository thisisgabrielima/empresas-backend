class CreateEnterprises < ActiveRecord::Migration[5.1]
  def change
    create_table :enterprises do |t|
      t.string :name
      t.text :description
      t.string :email
      t.string :facebook
      t.string :twitter
      t.string :linkedin
      t.string :phone
      t.string :own_enterprise
      t.string :photo
      t.integer :value
      t.integer :shares
      t.integer :share_price
      t.integer :own_shares
      t.string :city
      t.string :country

      t.timestamps
    end
  end
end
