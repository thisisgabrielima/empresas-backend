class EnterprisesController < ApplicationController

	protect_from_forgery with: :null_session

	def index

		if params[:enterprise_types].blank? && params[:name].blank?
			@enterprises = Enterprise.all

		elsif params[:enterprise_types].blank?

			@enterprises = Enterprise.where(name: params[:name])
	
		elsif params[:name].blank?
			@enterprises = Enterprise.where(enterprise_type_id: params[:enterprise_types])

		else
			@enterprises = Enterprise.where({name: params[:name], enterprise_type_id: params[:enterprise_types]})
		end

		render json: @enterprises, status: :ok
	end

	def show
		@enterprise = Enterprise.find(params[:id])

		render json: @enterprise, status: :ok
	end
end
